import React from 'react'
import {useState, useEffect, useContext} from 'react'
import { useNavigate } from 'react-router-dom'
import UserContext from './../UserContext'
// import {login} from "../redux/apiCalls";
// import {useDispatch , useSelector }from "react-redux"
import {Link} from 'react-router-dom';



import styled from "styled-components";


const Container = styled.div`
  width: 100vw;
  height: 100vh;
  background:
    url("https://media.gq.com/photos/6101bf7fb66e7d7e0dd4ab15/master/pass/sneaker-brands.jpg")
      center;
  background-size: cover;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Wrapper = styled.div`
  width: 25%;
  padding: 20px;
  background-color: white;

`;

const Title = styled.h1`
  font-size: 24px;
  font-weight: 300;
`;

const Form = styled.form`
  display: flex;
  flex-direction: column;
`;

const Input = styled.input`
  flex: 1;
  min-width: 40%;
  margin: 10px 0;
  padding: 10px;
`;

const Button = styled.button`
  width: 100%;
  border: none;
  padding: 15px 20px;
  background-color: red;
  color: white;
  cursor: pointer;
  margin-bottom: 10px;
  transition:all 1s ease;
  &:hover{
    background-color:pink;
    transform: scale(1.5);
  }
  &:disabled{
    color:transparent;
    cursor: not-allowed;
  }
`;

const Linek = styled.a`
  margin: 5px 0px;
  font-size: 12px;
  text-decoration: underline;
  cursor: pointer;
`;

// const Error =styled.span`
//   color: red
// `

const Login = () => {

  // const [username,setUsername] = useState("");

  // const [password,setPassword] = useState("");


  // const dispatch = useDispatch();


  // const {isFetching , error } = useSelector((state) => state.user);

  // const handleClickLogin = (e) => {
  //   e.preventDefault();
  //   login(dispatch, { username, password });
  // };

  const [email, setEmail] = useState("")
	const [pw, setPW] = useState("")
	const [isDisabled, setIsDisabled] = useState(true)

	const { state, dispatch } = useContext(UserContext)
	const navigate = useNavigate()

	useEffect(() => {
		if(email !== "" && pw !== ""){
			setIsDisabled(false)
		} else {
			setIsDisabled(true)
		}
	}, [email, pw])

	const loginUser = (e) => {
		e.preventDefault()

		fetch('https://cryptic-plains-85866.herokuapp.com/api/user/login', {
			method: "POST",
			headers:{
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: pw
			})
		})
		.then(response => response.json())
		.then(response => {
			console.log(response)

			if(response){
				localStorage.setItem('token', response.token)
				const token = localStorage.getItem("token")

				fetch('https://cryptic-plains-85866.herokuapp.com/api/user/profile', {
				      method: "GET",
				      headers:{
				        "Authorization": `Bearer ${token}`
				      }
				    })
				    .then(response => response.json())
				    .then(response => {
				      
				      localStorage.setItem('admin', response.isAdmin)

				      dispatch({type: "USER", payload: true})

				      navigate('/')
				        
				    })

				setEmail("")
				setPW("")

			} else {
				alert('Incorrect credentials!')
			}
		})
	}





  return (
    <Container>
      <Wrapper>
        <Title>SIGN IN</Title>
        <Form onSubmit={(e) => loginUser(e) }> 
          <Input placeholder="Email" onChange={(e)=> setEmail(e.target.value)}/>
          <Input placeholder="password"
          type="password" 
          onChange={(e)=> setPW(e.target.value)} />



          <Button variant="info" 
							type="submit"
							disabled={isDisabled}>LOGIN</Button>

          {/* {error && <Error>Something Went Wrong...</Error>} */}

          <Linek><Link to="/register">CREATE A NEW ACCOUNT</Link></Linek>
        </Form>
      </Wrapper>
    </Container>
  );
};

export default Login;