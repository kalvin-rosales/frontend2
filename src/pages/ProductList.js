import React from 'react'
import styled from 'styled-components'
import Navbar from '../components/Navbar'
import Announcement from "../components/Announcement"
import Products from "../components/Products"
import Footer from "../components/Footer"
import { useLocation } from 'react-router-dom';
import { useState } from 'react'


const Container = styled.div`

`;



const Title = styled.h1`
    margin: 20px;
`;



const FilterContainer = styled.div`
    display:flex;
    justify-content: space-between;
`;


const Filter = styled.div`
    margin: 20px;
`;

const FilterText = styled.span`
    font-size: 20px;
    font-weight:600;
    margin-right: 20px;
`

const Select = styled.select`
    padding: 10px;
    margin-right: 20px;
`;
const Option = styled.option`

`;



const ProductList = () => {
    const location = useLocation();
    console.log(location.pathname.split("/")[2]);

    const cat = location.pathname.split("/")[2];

    const [filter, setFilters] = useState({})
    const [ sort, setSort ] = useState("latest")

    const handleFilters = (e) => {
        const value = e.target.value;
        setFilters({
            ...filter,
            [e.target.name]:value,
        });
    };


    console.log(filter)

  return (
    <Container>
        <Announcement/>
        <Navbar/>
        <Title>{cat}</Title>
        <FilterContainer>
            <Filter>
                <FilterText>
                    Filter Products:
                </FilterText>
                
                <Select name="size" onChange={handleFilters}>
                    <Option disabled >
                        Size
                    </Option>
                    <Option>Small</Option>
                    <Option>Medium</Option>
                    <Option>Large</Option>
                </Select>   
            </Filter>
            <Filter>
                <FilterText>
                    Sort Products:
                </FilterText>
                <Select onChange={e => setSort(e.target.value)}>
                    <Option value ="latest">
                        Latest
                    </Option>
                    <Option value ="asc">Price (asc)</Option>
                    <Option value ="desc">Price (desc)</Option>
                </Select>
            </Filter>
        </FilterContainer>
        <Products cat={cat} filters={filter} sort={sort}/>
        <Footer/>
    </Container>
  )
}

export default ProductList
