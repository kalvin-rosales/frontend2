
import { Facebook, Instagram, Twitter, YouTube } from '@material-ui/icons'
import React from 'react'
import styled from 'styled-components'


const Container = styled.div`
    display:flex;
    background-color: black;
`
const Left = styled.div`
    flex:1;
    display:flex;
    flex-direction: column;
    padding:20px;
`
const Zuitt = styled.h1`
    color: white;
    
    text-align: center;
`;
const Desc = styled.p`
    margin:20px 0px;
    color: white;
    text-align: center;
`;
const SocialContainer = styled.div`
    display:flex;

`;
const SocialIcon = styled.div`
    width: 40px;
    height: 40px;
    border-radius: 50%;
    color: white;
    justify-content: center;
`;

const Center = styled.div`
    flex:1;
    text-allign: center;
`
const Right = styled.div`
    flex:1;
    justify-content: center;
`
const SelfInfo = styled.p`
    color: white;
    text-align: center;
`
const Footer = () => {
  return (
    <Container>
        <Left>
       
 
        <SocialContainer>
            <SocialIcon>
                <Facebook/>
            </SocialIcon>
            <SocialIcon>
                <Twitter/>
            </SocialIcon>
            <SocialIcon>
                <Instagram/>
            </SocialIcon>
            <SocialIcon>
                <YouTube/>
            </SocialIcon>
        </SocialContainer>

        </Left>
        <Center> <Zuitt>Zuitt Training Bootcamp capstone 3| Kalvin Rosales </Zuitt>
        <Desc>
                The purpose of this project is for educational learning only

            </Desc>
        </Center>
        <Right>
        <SelfInfo>Kalvin Philip Rosales</SelfInfo>
        <SelfInfo>54 Alfred street Kingsville Antipolo Rizal</SelfInfo>
        <SelfInfo>09273989399</SelfInfo>
        
        </Right>
        
    </Container>
  )
}

export default Footer
