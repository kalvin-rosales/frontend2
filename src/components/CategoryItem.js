import React from 'react'
import { Link } from 'react-router-dom'
import styled from "styled-components"

const Container = styled.div`
    flex:1;
    margin: 3px;
    height: 70vh;
    position: relative;
`



const Image = styled.img`
   display: flex;
   justify-content: center;
    object-fit: cover;
`

const Info = styled.div`
    position: absolute;
    top: 0;
    left:0;
    width:100%;
    height:100%;
    display: flex;
    flex-direction: column;
    allign-items: center;
    justify-content: center;
`

const Title = styled.h1`
    color:black;
    margin-button:20px;
    text-allign: center;

`

const Button = styled.button`
    border:none;
    padding:10px;
    background-color: black;
    color: white;
    cursor: pointer;
    font-weight:600;
`


const CategoryItem = ({item}) => {
  return (
    <Container>
    <Link to={`/products/${item.cat}`}>
        <Image src={item.img}/>
        <Info>
            <Title>{item.title}</Title>
            <Button>BUY NOW</Button>
        </Info>
    </Link>
    </Container>
  )
}


export default CategoryItem
